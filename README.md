Voici l'API de l'école national du Numérique !!!

Pour pouvoir utiliser l'API suivez les instructions suivantes:

- Clonez le repo Gitlab,
- Ouvrez un nouveau terminal et lancez le serveur avec la commande: php -S localhost:8000
- Pour pouvoir consulter les informations de son enfant avec son ID, veuillez cliquer sur l'url suivante: http://localhost:8000/studients.php?idstudents=

ATTENTION !!!! Voici les id des éléves disponibles (2, 3, 4, 5). 
Merci de rentrer un de ses chiffres aprés le égal dans l'URL. Pour exemple :  http://localhost:8000/studients.php?idstudents=2

- Pour pouvoir accéder à nos programmes et consulter les cours dispensés ainsi que l'horaire et le professeur, veuillez cliquer sur l'url suivante : http://localhost:8000/programs.php?idprograms=

Dans notre école 4 programmes sont disponibles. Pour les consulter, merci de rentrer un chiffre entre 1 et 4 à la fin de l'url comme dans l'exemple suivant : http://localhost:8000/programs.php?idprograms=1

- En tant qu'administrateur, vous souhaitez connaître l'emploi du temps de chaque salles dans votre école, veuillez cliquer sur l'url suivant en fonction de l'id de la salle:
- http://localhost:8000/classrooms.php?idclassrooms=

Il y a 15 salles disponibles donc n'oubliez pas de rentrer un chiffre entre 1 à 15 aprés le égal comme dans l'exemple suivant: http://localhost:8000/classrooms.php?idclassrooms=1

Voilà MERCI de votre compréhension !!!
