<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Programs</title>
</head>

<body>



    <?php

    include("connection.php");
    $id = $_GET['idprograms'];
    $query = ("SELECT programs.entitled AS programme, courses.entitled AS cours, `last_name`, `name`, `date`, `time` FROM magie.programs_has_courses 
    INNER JOIN magie.programs ON magie.programs_has_courses.programs_idprograms = magie.programs.idprograms
    INNER JOIN magie.courses ON magie.programs_has_courses.courses_idcourses = magie.courses.idcourses
    INNER JOIN magie.classrooms ON magie.programs_has_courses.courses_classrooms_idclassrooms = magie.classrooms.idclassrooms
    INNER JOIN magie.professors ON magie.programs_has_courses.courses_professors_idprofessors = magie.professors.idprofessors
    WHERE programs.idprograms = ($id); ");
    $result = mysqli_query($dsn, $query) or die("Error in Selecting " . mysqli_error($dsn));
    $emparray = array();

    while ($row = mysqli_fetch_assoc($result)) {
        $emparray[] = $row;
    }
    ?>
    <?php
    echo json_encode($emparray);
    ?>

</body>

</html>